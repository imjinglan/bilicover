# 哔哩哔哩视频信息下载

## 介绍
通过哔哩哔哩API下载视频的信息与封面

## 使用说明

### 使用前

使用前务必安装以下运行库

```
Python3
```

在命令行中执行以下指令

```python
pip install requests
```



### 如何使用

#### 通用方法

你可以clone本仓库

```shell
git clone https://gitee.com/imjinglan/bilicover.git
```

然后使用 python 指令执行

#### Windows

如果你是Windows 用户，我建议你使用打包好的.exe版本，你可以在这里找到

[RELEASES](https://gitee.com/imjinglan/bilicover/releases)

#### MACOS & LINUX

如果你是非Windows用户，请参照**通用方法**



### 引用库

- requests
- json
- re
- os
- sys



### 使用语言

![PYTHON 100%](https://img.shields.io/badge/PYTHON-100%25-success?style=flat-square)

### 版权信息
**CopyRight (c) ImJingLan 2021**